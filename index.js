//Load express
var express = require ("express");

//Create an application
var app = express();
//Load express-handlebars
var handlebars = require("express-handlebars");
//Ask our app to use handlebars as the template engine
//Default layout to use is main (main.handlebars)

app.engine("handlebars",
    handlebars({
        defaultLayout: "index",
        helpers: {
            isActive: function(view, actual){
                if (view == actual)
                    return ("active");
                return ("");
            }
        }

}));
app.set("view engine", "handlebars");

var config = {
    connectionLimit: 5,
    host: "173.194.229.7",
    user: "ttc",
    password: "ttc",
    database: "ttc_db"
};

//var config = require("./modules/dbconfig");

var mysql =require("mysql");
var connPool = mysql.createPool(config);

var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/",function(req, res) {
    res.render("index");
});
    
app.get("/query",function (req,res) {
    console.info("query connected");
    connPool.getConnection(function (err, conn) {
        if (err) {
            console.error("get connection error: %s", err);
            res.render("index");
            return;
        }
        var q = "select * from games where Venue like '%" +
            req.query.venue + "%'";
            var paramQuery = "select * from games where Venue like ? ";
            console.info("query: %s", q);
        try {
        conn.query(paramQuery,
            ["%" + req.query.venue + "%"], //actual parameters based on position
            function (err, rows) {
                if (err) {
                    console.error("get connection error: %s", err);
                    res.render("index");
                    return;
                }
                res.render("join", {
                    games: rows
                });
            });
            } catch (e) {
            } finally {

                conn.release();

            }
        });
});

app.get("/add",function (req,res) {
    console.info("query connected");
    connPool.getConnection(function (err, conn) {
        if (err) {
            console.error("get connection error: %s", err);
            res.render("index");
            return;
        }
        var q = "select * from games where Venue like '%" +
            req.query.venue + "%'";
        var paramQuery = "select * from games where venue like ? ";
        console.info("query: %s", q);
        try {
            conn.query(paramQuery,
                ["%" + req.query.venue + "%"], //actual parameters based on position
                function (err, rows) {
                    if (err) {
                        console.error("get connection error: %s", err);
                        res.render("index");
                        return;
                    }
                    res.render("join", {
                        games: rows
                    });
                });
        } catch (e) {
        } finally {

            conn.release();

        }
    });
});

app.post("/post", function (req, res) {
    console.info(">>add game working");
    console.info("Game ID: %s", req.body.GameID);
    console.info("Date: %s", req.body.thisdate);
    console.info("Time: %s", req.body.Time);
    console.info("Venue: %s", req.body.Venue);
    console.info("Pitch: %s", req.body.Pitch);
    console.info("Duration: %s", req.body.duration);
    console.info("Cost per Player: %s", req.body.Gamecost);

    connPool.getConnection(function (err, conn) {
        if (err) {
            console.error("get connection error: %s", err);
            res.render("postgame");
            return;
        }
      //  var s = "insert into games values ('%" +
         //   req.body.GameID + "%'" + ", '%" + req.body.thisdate + "%'" +
          //  ", '%" + req.body.Time + "%'" + ", '%" + req.body.Venue + "%'" +
          //  ", '%" + req.body.Pitch + "%'" + ", '%" + req.body.duration + "%'" +
          //      ", '%" + req.body.Gamecost + "%')"
            // conn.query ("insert into table values values (?,?,?)",
            //[req.body.GameID, req.body],

        try {
            conn.query("insert into games values (?, ?, ?, ? ,? ,? ,?)",
                [req.body.GameID, req.body.thisdate, req.body.Time, req.body.Venue, req.body.Pitch, req.body.duration, req.body.Gamecost],
                //actual parameters based on position
                function (err, result) {
                    conn.release();
                    res.redirect("/menu/postgame");
                })
        }
     catch (e) {
    } finally {
        }
 });
});

app.get("/menu/:category", function (req, res) {
    console.info(">>> category %s", req.params.category);
    var cat = req.params.category;
    switch (cat) {
        case "index":
            connPool.getConnection(function (err, conn) {
                if (err) {
                    console.error ("get connection error: %s", err);
                    res.render ("index");
                    return;
                }
                conn.query("SELECT * FROM ttc_db.games",
                    function (err, rows) {
                    try {
                        if (err) {
                            console.error("get connection error: %s", err);
                            res.render("index");
                            return;
                        }
                        console.error ("running index", err);
                        res.render("index", {
                            games : rows
                        });
                    } catch (e) {
                        } finally {
                            conn.release();
                        }

                    });
            });
            break;
        
        case "join":
            connPool.getConnection(function(err,conn) {
                if (err) {
                    console.error ("get connection error: %s", err);
                    res.render ("index");
                    return;
                }
                conn.query("SELECT * FROM ttc_db.games", function(err,rows) {
                    try {
                        if (err) {
                            console.error("get connection error: %s", err);
                            res.render("index");
                            return;
                        }
                        res.render("join", {
                            games:rows
                        });
                    }catch (e) {
                    } finally {

                        conn.release();
                    }
                });
            });
            break;
        case "signup":
            res.render(cat);
            break;
        case "postgame":
            connPool.getConnection(function(err,conn) {
                if (err) {
                    console.error ("get connection error: %s", err);
                    res.render ("index");
                    return;
                }
                conn.query("SELECT * FROM ttc_db.customers", function(err,rows) {
                    try {
                        if (err) {
                            console.error("get connection error: %s", err);
                            res.render("index");
                            return;
                        }
                        res.render("postgame", {
                            games: rows
                        });
                    }catch (e) {
                    } finally {
                        
                    conn.release();

                }
            });
            });
            break;
        default:
            res.redirect("/");
    }
});
//app.get("/join",function(req,res){
  //  res.render("join")
//})
//Process incoming request


//Set the public/static directory
app.use(express.static(__dirname+ "/public"));
//Catch all
app.use(function(req, res, next){
    console.error("File not found: %s", req.originalUrl);
    res.redirect("/");
});

app.set("port", process.env.APP_PORT || 3000);

//Create a landing page in public dir
app.listen(3000,function(){
    
    console.info("Application is listening on port 3000")
});
//Start the server
